### Your first project DevOps!

### Build Docker image from current directory
````shell
docker build --tag website .
````

### Run docker's image
````shell
docker run --publish 80:80 website
````

### Run Unit tests
````shell
docker-compose run --rm unit-tests
````

### Run selenium
````shell
docker-compose up -d website selenium
````

### Terraform
https://www.terraform.io/

--rm Automatically remove the container when it exists
````shell
docker build --tag terraform --file terraform.Dockerfile .
docker run -rm terraform -version
docker run  --rm --interactive --tty --entrypoint sh terraform
terraform
# use the following command to re-build the particular service in docker-compose.yml file
docker-compose build terraform
# use the following command to run the particular service in docker-compose.yml file
docker-compose run --rm terraform
````

#### Reviewing the Terraform plan
````shell
docker-compose run --rm terraform init

# the following command is used to see the terraform STATE
# the terraform state can keep the sensitive information like passwords etc
ls .terraform/terraform.tfstate

````

#### Applying the Terraform plan
````shell
docker-compose run --rm terraform plan

# You will be asked about:
- region : us-east-1
# Provide the following variables as UNIX variables
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

docker-compose run --rm terraform apply
````

#### Deploying the website into AWS S3
````shell
docker-compose run --rm aws
docker-compose run --rm --entrypoint aws aws

docker-compose run --rm --entrypoint aws aws ec2 describe-instances
# the following command to get BUCKET NAME
docker-compose run --rm terraform output
# DOES NOT WORK
docker-compose run --rm -e PAGER=less --entrypoint aws aws s3 help

# copy website to S3 - put here BUCKET NAME
docker-compose run --rm --entrypoint aws aws s3 cp --recursive website/ s3://explorecalifornia.org
````

### Destroying the website from AWS S3
```shell
docker-compose run --rm --entrypoint aws aws s3 rm s3://explorecalifornia.org --recursive 
docker-compose run --rm terraform destroy

# after that It has sense to verify files existing 
ls -la
# and see: terraform.tfstate
```

#### Writing your integration test & Running your integration test
````shell
#upd website_spec.rb
# run terraform
docker-compose run --rm terraform plan

docker-compose run --rm terrafrom apply
docker-compose run --rm --entrypoint aws aws s3 cp --recursive website/ s3://explorecalifornia.org

# modify docker-compose.yml
# add service: integration-tests

docker-compose up -d selenium
# then Screen Sharing 
# vnc://localhost:5901
# provide password

docker-compose run --rm integration-tests

# get output
docker-compose run --rm terraform output

docker-compose run --rm -e WEBSITE_URL=<OUTPUT> integration-tests
````

#### Installing Jenkins on Docker & Writing a Jenkinsfile for the app
see plugins.jenkins.io
````shell
# create jenkins.Dockerfile and plugins.txt
docker-compose up jenkins
# find password: at /var/jenkins-home/secrets/initialAdminPassword
http://localhost:8080


# create Jenkinsfile
see https://jenkins.io/doc/book/pipeline/syntax
# Declarative and Scripted pipelines

````

#### Using Jenkinsfile to deploy your app
````shell
# create the job of the type: Multibranch pipeline
Add source: Git
file:///app

Build configuration:
Script path: Jenkinsfile

````