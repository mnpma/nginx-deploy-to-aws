FROM ubuntu
MAINTAINER MP
#RUN wget -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && \
RUN apt-get update && apt-get -y install gnupg2
RUN apt-get update && apt-get -y install curl
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -

RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

RUN apt-get update && apt-get -y install terraform

# RUN unzip /tmp/terraform.zip -d /
#RUN apk add --no-cache ca-certificates curl
USER nobody
ENTRYPOINT [ "/terraform" ]
