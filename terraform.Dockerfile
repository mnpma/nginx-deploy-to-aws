FROM hashicorp/terraform
MAINTAINER MP
RUN apk add --no-cache ca-certificates curl
USER nobody
ENTRYPOINT [ "/terraform" ]
